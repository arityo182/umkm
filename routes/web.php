<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/api', [AuthController::class, 'index']);

Route::get('/{vue_capture?}', function () {
    return view('welcome');
})->where('vue_capture', '[\/\w\.-]*');

Route::get('verify_mail/{token}', [AuthController::class, 'verificationMail']);
// Route::get('/reset-password/{token}', [AuthController::class, 'resetPasswordLoad']);
// Route::get('/reset-password/{id}', [AuthController::class, 'resetPassword']);

// Route::fallback(function () {
//     //
//     return response()->json(['description' => 'Tidak ditemukan', 'status_code' => 404], 404);
// });
