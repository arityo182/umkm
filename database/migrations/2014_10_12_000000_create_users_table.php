<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('user_id');
            $table->string('username')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('is_verified')->default(0)->comment('0 = not verifed, 1 = verified');
            $table->string('password');
            $table->string('no_telp')->unique();
            $table->string('status_user')->default(1)->comment('0 = not active, 1 = pembeli, 2 = penjual, 3 = admin');
            $table->rememberToken();
            $table->timestamps();
            $table->datetime('last_login_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
