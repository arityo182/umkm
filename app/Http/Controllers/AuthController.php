<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\PasswordReset;
use Exception;
use Facade\FlareClient\View;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\URL;

require_once '/xampp/htdocs/UMKM/vendor/autoload.php';

use Twilio\Rest\Client;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('JWT', ['except' => [
            'login', 'register', 'verifyOTP', 'sendVerifyMail', 'verificationMail',
            'forgotPassword', 'resetPasswordLoad', 'resetPassword', 'index'
        ]]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    #region >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> AUTH AREA (JWT) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


    public function index()
    {
        return response("Backend UMKM");
    }

    public function login(Request $request)
    {
        try {
            // Get data from request body
            $credentials = request(['email', 'password']);

            // check mandatory data
            $validateData = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required',
            ]);
            if ($validateData->fails()) {
                return $this->parameter_error($validateData->errors());
            }

            // Check Credential in Database and Login
            if (!auth()->attempt($credentials)) {
                return $this->defined_error("Email not Restered or Not Active", "Invalid Credential", 401);
            }
            // if (Auth::user()->is_verified == "0") {
            //     return $this->defined_error("Email is not Verified", "Invalid Credential", 401);
            // }
            $email = $request->email;
            if (Auth::user()->email == $email) {
                $userPhone = Auth::user()->no_telp;
                // $username = Auth::user()->username;
                $otp = mt_rand(1000, 9999);

                // try {
                //     $basic  = new \Nexmo\Client\Credentials\Basic(getenv("NEXMO_KEY"), getenv("NEXMO_SECRET"));
                //     $client = new \Nexmo\Client($basic);

                //     $receiverNumber = "6282226151892";
                //     $message = 'Kode Keamanan untuk UMKM :' . $otp;
                //     $message = "This is testing from ItSolutionStuff.com";

                //     $message = $client->message()->send([
                //         'to' => $receiverNumber,
                //         'from' => 'Vonage APIs',
                //         'text' => $message
                //     ]);

                //     // $client->message()->send([
                //     //     'to' => $userPhone,
                //     //     'from' => 'Vonage APIs',
                //     //     'text' => 'Kode Keamanan untuk UMKM :' . $otp
                //     // ]);

                //     $userid = User::where('no_telp', $userPhone)->get();
                //     $user = User::find($userid[0]['user_id']);
                //     $user->remember_token = $otp;
                //     $user->save();

                //     return response()->json(['no_telp' => $userPhone, 'password' => $request->password, 'description' => 'code has been sent', 'status_code' => 200], 200);
                // } catch (Exception $e) {
                //     return $this->bad_request(strtolower($e->getMessage()));
                // }
                try {
                    // $account_sid = getenv("TWILIO_SID");
                    // $auth_token = getenv("TWILIO_AUTH_TOKEN");
                    // $twilio_number = getenv("TWILIO_NUMBER");
                    // // $account_sid = "ACf797d1afc9ea4af30b952fa945af2f85";
                    // // $auth_token = "9b04bdf945855b9d8785082f29708976";
                    // // $twilio_number = "+19404569880";
                    // $client = new Client($account_sid, $auth_token);
                    // $client->messages->create($userPhone, [
                    //     'from' => $twilio_number,
                    //     'body' => 'Kode Keamanan untuk UMKM :' . $otp
                    // ]);
                    $userid = User::where('no_telp', $userPhone)->get();
                    $user = User::find($userid[0]['user_id']);
                    // print_r($user);
                    $user->remember_token = $otp;
                    $user->save();
                    // $username = Auth::user()->username;
                    // $user2 = User::find($userName[0]['user_id']);
                    return response()->json(['email' => $request->email, 'password' => $request->password, 'description' => 'Code has been sent', 'status_code' => 200], 200);
                } catch (Exception $e) {
                    return $this->bad_request(strtolower($e->getMessage()));
                }
            }
            // return $this->respondWithToken($token);
            // return response()->json(['username' => Auth::user()->username, 'description' => 'succes', 'status_code' => 200], 200);
        } catch (Exception $e) {
            return $this->bad_request(strtolower($e->getMessage()));
        }
    }

    public function verifyOTP(Request $request)
    {
        try {
            $validateData = Validator::make($request->all(), [
                'email' => 'required',
                'otp_code' => 'required|size:4',
                'password' => 'required',
            ]);
            if ($validateData->fails()) {
                return $this->parameter_error($validateData->errors());
            }

            $otp_code = $request->otp_code;
            $credentials = request(['email', 'password']);
            $user = User::where('remember_token', $otp_code)->get();
            if (count($user) > 0) {
                // $datetime = Carbon::now()->format('Y-m-d H:i:s');
                $user = User::find($user[0]['user_id']);
                $user->remember_token = '';
                $user->save();
                return $this->respondWithToken(auth()->attempt($credentials));
            } else {
                return $this->defined_error("Invalid OTP Code", "Invalid Credential", 401);
            }
        } catch (Exception $e) {
            return $this->bad_request(strtolower($e->getMessage()));
        }
    }

    public function register(Request $request)
    {
        try {
            $validateData = Validator::make($request->all(), [
                'email' => 'required|unique:users|max:255',
                'username' => 'required|unique:users|max:255',
                'name' => 'required',
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6|required_with:password|same:password',
                'no_telp' => 'required|unique:users|min:10',
            ]);
            if ($validateData->fails()) {
                return $this->parameter_error($validateData->errors());
            }
            $data = array();
            $data['name'] = $request->name;
            $data['username'] = $request->username;
            $data['email'] = $request->email;
            $data['password'] = Hash::make($request->password);
            $data['no_telp'] = $request->no_telp;
            $data['created_at'] = Carbon::now()->toDateTimeString();
            // $data['updated_at'] = Carbon::now()->toDateTimeString();
            DB::table('users')->insert($data);
            return response()->json(['description' => ' Registration success', 'status_code' => 200], 200);
        } catch (Exception $e) {
            return $this->bad_request(strtolower($e->getMessage()));
        }
    }

    public function sendVerifyMail($email)
    {
        try {
            if (!auth()->user()) {
                $user = User::where('email', $email)->get();
                if (count($user) > 0) {
                    // if ($request->email) {
                    $random = Str::random(40);
                    $domain = URL::to('/');
                    $url = $domain . '/api/auth/verificationMail/' . $random;

                    $data['url'] = $url;
                    $data['email'] = $email;
                    $data['title'] = "Email Verification";
                    $data['body'] = "Please click here to below to verify your email";

                    Mail::send('verifyMail', ['data' => $data], function ($message) use ($data) {
                        $message->to($data['email'])->subject($data['title']);
                    });

                    $user = User::find($user[0]['id']);
                    $user->remember_token = $random;
                    $user->save();
                    return response()->json(['description' => 'Sent Email Verify Successfully', 'status_code' => 200], 200);
                }
            } else {
                return response()->json(['description' => 'User is not Authenticated', 'status_code' => 401], 401);
            }
        } catch (Exception $e) {
            $this->bad_request(strtolower($e->getMessage()));
        }
    }

    public function verificationMail($token)
    {
        try {
            $user = User::where('remember_token', $token)->get();
            if (count($user) > 0) {
                $datetime = Carbon::now()->format('Y-m-d H:i:s');
                $user = User::find($user[0]['id']);
                $user->remember_token = '';
                $user->is_verified = 1;
                $user->email_verified_at = $datetime;
                $user->save();
                return response()->json(['messege' => 'success verification email', 'status_code' => 200], 200);
            } else {
                return View('404');
            }
        } catch (Exception $e) {
            $this->bad_request(strtolower($e->getMessage()));
        }
    }


    public function forgotPassword(Request $request)
    {
        try {
            $validateData = Validator::make($request->all(), [
                'email' => 'required|max:255',
            ]);
            if ($validateData->fails()) {
                return $this->parameter_error($validateData->errors());
            }
            $user = User::where('email', $request->email)->get();
            if (count($user) > 0) {
                $token = Str::random(40);
                $domain = URL::to('/');
                $url = $domain . '/validation-password?token=' . $token;

                $data['url'] = $url;
                $data['email'] = $request->email;
                $data['title'] = "Password Reset";
                $data['body'] = "Please click on below link to reset your password";

                // Mail::send('forgetPasswordMail', ['data' => $data], function ($message) use ($data) {
                //     $message->to($data['email'])->subject($data['title']);
                // });

                $datetime = Carbon::now()->format('Y-m-d H:i:s');

                $data = array();
                $data['email'] = $request->email;
                $data['token'] = $token;
                $data['created_at'] = $datetime;
                PasswordReset::where('email', $data)->delete();
                DB::table('password_resets')->insert($data);
                // PasswordReset::create([
                //     ['email' => $request->email],
                //     [
                //         'email' => $request->email,
                //         'token' => $token,
                //         'created_at' => $datetime
                //     ]
                // ]);
                return response()->json(['description' => 'Succes send reset reset email', 'status_code' => 200], 200);
            } else {
                return $this->defined_error("User Not Found", "invalid Credential", 401);
            }
        } catch (Exception $e) {
            return $this->bad_request(strtolower($e->getMessage()));
        }
    }

    public function resetPasswordLoad($token)
    {
        try {
            // $resetData = DB::table('password_resets')->select('email')->where('token', $token)->first();
            $resetData = DB::table('password_resets')->where('token', $token)->pluck('email');
            // $resetData2 = PasswordReset::where('token', $token)->get();
            // return print_r($resetData);
            if (isset($token) && count($resetData) > 0) {
                // $user = User::where('email', $resetData[0]['email'])->get();
                $user = User::where('email', $resetData[0])->get();
                // $user = DB::table('users')->select('email')->where('email', $resetData)->first();
                // return print_r($user);
                return response()->json(['description' => $user[0], 'status_code' => 200], 200);
                // return view(route('/validation-password'), compact('user'));
            } else {
                return $this->defined_error("Invalid Token", "Invalid Credential", 401);
            }
        } catch (Exception $e) {
            return $this->bad_request(strtolower($e->getMessage()));
        }
    }

    public function resetPassword(Request $request)
    {
        try {
            $validateData = Validator::make($request->all(), [
                // 'email' => 'required|max:255',
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6|required_with:password|same:password',
            ]);
            if ($validateData->fails()) {
                return $this->parameter_error($validateData->errors());
            }
            $user = User::find($request->user_id);
            $user->password = Hash::make($request->password);
            $user->save();
            PasswordReset::where('email', $user->email)->delete();
            // $credentials = request(['email', 'password']);
            return response()->json(['description' => 'Succes change password']);
            // return $this->respondWithToken(auth()->attempt($credentials));
        } catch (Exception $e) {
            return $this->bad_request(strtolower($e->getMessage()));
        }
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        try {
            auth()->user();
            return response()->json([auth()->user(), 'status_code' => 200], 200);
        } catch (Exception $e) {
            return $this->bad_request(strtolower($e->getMessage()));
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            auth()->logout();
            return response()->json(['message' => 'Successfully logged out', 'status_code' => 200], 200);
        } catch (Exception $e) {
            return $this->bad_request(strtolower($e->getMessage()));
        }
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(JWTAuth::refresh());
    }

    #end region >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> AUTH AREA (JWT) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


    # region >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FUNCTION AREA <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            // 'userInfo' => Auth::user(),
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => JWTAuth::factory()->getTTL() * 60,
            'status_code' => 200,
            'name' => auth()->user()->name,
            // 'user_id' => auth()->user()->id,
            // 'email' => auth()->user()->email,
        ]);
    }


    protected function permission_failed()
    {
        return response()->json(['error' => 'Perssion Failed', 'status_code' => 403], 403);
    }

    protected function request_failed()
    {
        return response()->json(['error' => 'Request Failed', 'status_code' => 403], 403);
    }

    protected function defined_error($description, $error = "Defined Error", $status_code = 499)
    {
        return response()->json(['description' => $description, 'error' => $error, 'status_code' => $status_code], $status_code);
    }

    protected function parameter_error($description, $error = "Defined Error", $status_code = 400)
    {
        if (App::environment('local')) {
            return response()->json(['description' => $description, 'error' => $error, 'status_code' => $status_code], $status_code);
        } else {
            return response()->json(['description' => "Terjadi Kesalahan Sistem", 'error' => $error, 'status_code' => $status_code], $status_code);
        }
    }

    protected function bad_request($description)
    {
        if (App::environment('local')) {
            return response()->json(['description' => $description, 'error' => 'Bad Request', 'status_code' => 400], 400);
        } else {
            return response()->json(['description' => "Terjadi Kesalahan Sistem", 'error' => 'Bad Request', 'status_code' => 400], 400);
        }
    }
    # end region >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FUNCTION AREA <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
}
