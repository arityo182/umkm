// Authentification
let login = require('./components/auth/login.vue').default;
let register = require('./components/auth/register.vue').default;
let forgot = require('./components/auth/forgotInputEmail.vue').default;
let logout = require('./components/auth/logout.vue').default;
let forgotpasswordfrom = require('./components/auth/forgotInputPassword.vue').default;
let validationpassword = require('./components/auth/forgotInputPassword.vue').default;
let loginotp = require('./components/auth/loginotp.vue').default;
let homeUser = require('./components/home/main.vue').default;
let notFound = require('./components/notFound.vue').default;

// End Autentification
// let home = require('./components/home.vue').default;


export const routes = [
    { path: '/login', component: login, name: '/login', meta: {title:"UMKM | Login"}},
    { path: '/register', component: register, name:'register', meta: {title:"UMKM | Register"}},
    { path: '/forgot', component: forgot, name:'forget'},
    { path: '/home', component: homeUser, name:'homeUser'},
    { path: '/logout', component: logout, name:'logout'},
    { path: '/reset-password?token=/', component: forgotpasswordfrom, name:'forgotpasswordfrom', meta: {title:"UMKM | Forgot Password"}},
    { path: '/validation-password', component: forgotpasswordfrom, name:'validationpassword', meta: {title:"UMKM | New Password"}},
    { path: '/loginotp', component: loginotp, name:'loginotp', meta: {title:"UMKM | Validation OTP"}},
    { path: '/:catchAll(.*)*', component: notFound, name:'notFound', meta: {title:"UMKM | Not Found"}},
]