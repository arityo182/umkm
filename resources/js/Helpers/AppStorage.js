class AppStorage {

    storeToken(token) {
        localStorage.setItem('token', token);
    }

    storeUserId(user_id) {
        localStorage.setItem('user_id', user_id)
    }

    storeUser(user) {
        localStorage.setItem('user', user);
    }

    storeEmail(email) {
        localStorage.setItem('email', email);
    }

    storepassword(password) {
        localStorage.setItem('password', password);
    }

    storeBeforeOtp(email, password) {
        this.storeEmail(email)
        this.storepassword(password)
    }

    storeResetPassword(user_id) {
        this.storeUserId(user_id)
    }

    store(token, user) {
        this.storeToken(token)
        this.storeUser(user)
    }
    clear() {
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        localStorage.removeItem('no_telp')
        localStorage.removeItem('password')
    }
    getToken() {
        localStorage.getItem(token);
    }
    getUser() {
        localStorage.getItem(user);
    }

}
export default AppStorage = new AppStorage(); 