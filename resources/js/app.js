require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// Router import
import {routes} from './routes';

// Import User class
import User from './Helpers/User';
window.User = User

// Sweet Alert start
import Swal from 'sweetalert2'
import { title } from 'process';
window.Swal = Swal;

const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
});

window.toast = toast;

const router = new VueRouter({
    routes, //short for 'routers: routes'
    mode: 'history'
})

router.beforeEach((to, from, next) =>{
    document.title = `${to.meta.title}`;
    next();
})

const app = new Vue({
    el: '#app',
    router
});
