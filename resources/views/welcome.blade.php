<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link href="{{ asset('backend/img/logo/logo.png')}}" rel="icon"> -->
    <title>RuangAdmin - Dashboard</title>
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css')}}"> -->
    <!-- <link href="{{ asset('backend/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css"> -->
    <!-- <link href="{{ asset('backend/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"> -->
    <!-- <link href="{{ asset('backend/css/ruang-admin.min.css')}}" rel="stylesheet"> -->
</head>

<body id="page-top">
    <div id="app">
        <router-view></router-view>
    </div>
    <!-- Scroll to top -->
    <!-- <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a> -->

    <script src="{{ asset('js/app.js')}}"></script>
    <!-- <script src="{{ asset('backend/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('backend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('backend/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('backend/js/ruang-admin.min.js')}}"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
    <!-- <link rel="stylesheet" type="text/css" href="./client/assets/css/styles.css"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('client/assets/css/styles.css')}}">
    <!-- Font google-->
    <!--Georama-->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100&display=swap" rel="stylesheet" />
    <!-- Goldman-->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100&display=swap" rel="stylesheet" />
    <!-- <script src="{{ asset('backend/vendor/chart.js/Chart.min.js')}}"></script>
    <script src="{{ asset('backend/js/demo/chart-area-demo.js')}}"></script> -->

    <script type="text/javascript">
        let token = localStorage.getItem('token');
        if (token) {
            $("#sidebar").css("display", "");
            $("#topbar").css("display", "");
        }
    </script>
</body>

</html>